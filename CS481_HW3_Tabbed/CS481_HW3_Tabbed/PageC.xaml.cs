﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageC : ContentPage
	{

        private Color funColor = Color.Black;
        public Color FunColor
        {
            get
            {
                return funColor;
            }
            set
            {
                funColor = value;
                OnPropertyChanged("FunColor");
            }

        }

        private double r = 0;
        public double R
        {
            get
            {
                return r;
            }
            set
            {
                r = value;
                OnPropertyChanged("R");
            }

        }

        private double g = 0;
        public double G
        {
            get
            {
                return g;
            }
            set
            {
                g = value;
                OnPropertyChanged("G");
            }

        }

        private double b = 0;
        public double B
        {
            get
            {
                return b;
            }
            set
            {
                b = value;
                OnPropertyChanged("B");
            }

        }

        //slider track color (before thumb)
        private string minTC = "Black";
        public string MinTC
        {
            get
            {
                return minTC;
            }
            set
            {
                minTC = value;
                OnPropertyChanged("MinTC");
            }

        }

        //slider track color (after thumb)
        private string maxTC = "#222222";
        public string MaxTC
        {
            get
            {
                return maxTC;
            }
            set
            {
                maxTC = value;
                OnPropertyChanged("MaxTC");
            }

        }

        public PageC ()
		{
			InitializeComponent ();
        }

        void OnAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page C has appeared.");
            BindingContext = this;
            CheckDarkMode();
        }

        void OnDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page C has disappeared.");
            
        }

        void ToggleDarkMode(object sender, EventArgs args)
        {
            MainPage.darkMode = !MainPage.darkMode;
            CheckDarkMode();
        }

        void CheckDarkMode()
        {
            if (MainPage.darkMode)
            {
                BackgroundColor = Color.Black;
                MinTC = "White";
                MaxTC = "LightGray";
            }
            else
            {
                BackgroundColor = Color.White;
                MinTC = "Black";
                MaxTC = "#222222";
            }
        }

        void OnRChanged(object sender, ValueChangedEventArgs args)
        {
            R = args.NewValue;
            UpdateColors();
        }

        void OnGChanged(object sender, ValueChangedEventArgs args)
        {
            G = args.NewValue;
            UpdateColors();
        }

        void OnBChanged(object sender, ValueChangedEventArgs args)
        {
            B = args.NewValue;
            UpdateColors();
        }

        void UpdateColors()
        {
            FunColor = new Color(R, G, B);
        }
    }
}