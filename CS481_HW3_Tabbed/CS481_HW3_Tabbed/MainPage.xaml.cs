﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace CS481_HW3_Tabbed
{
    public partial class MainPage : TabbedPage
    {
        public static bool darkMode = false;

        public MainPage()
        {
            InitializeComponent();
            Children.Add(new PageA());
            Children.Add(new PageB());
            Children.Add(new PageC());
            Children.Add(new PageD());
        }

        void OnAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("MainPage has appeared.");
        }

        void OnDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("MainPage has disappeared.");
        }
    }
}
