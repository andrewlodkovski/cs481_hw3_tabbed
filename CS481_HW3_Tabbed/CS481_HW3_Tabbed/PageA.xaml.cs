﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageA : ContentPage
	{

        private string text = "";
        public string Text 
        {
            get {
                return text;
            }
            set {
                text = value;
                OnPropertyChanged("Text");
            }

        }
        private string textColor = "Black";
        public string TextColor
        {
            get
            {
                return textColor;
            }
            set
            {
                textColor = value;
                OnPropertyChanged("TextColor");
            }

        }

        //slider track color (before thumb)
        private string minTC = "Black";
        public string MinTC
        {
            get
            {
                return minTC;
            }
            set
            {
                minTC = value;
                OnPropertyChanged("MinTC");
            }

        }

        //slider track color (after thumb)
        private string maxTC = "#222222";
        public string MaxTC
        {
            get
            {
                return maxTC;
            }
            set
            {
                maxTC = value;
                OnPropertyChanged("MaxTC");
            }

        }

        public PageA ()
		{
			InitializeComponent ();
		}

        //This is called in all pages, and all pages verify the dark mode when appearing.
        void OnAppearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page A has appeared.");
            BindingContext = this;
            Text = "";
            UpdateText(5);
            CheckDarkMode();
        }

        void OnDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page A has disappeared.");
        }

        //Dark Mode button press event
        void ToggleDarkMode(object sender, EventArgs args)
        {
            MainPage.darkMode = !MainPage.darkMode;
            CheckDarkMode();
        }

        //Checking whether DarkMode is enabled or not and making the appropriate changes
        void CheckDarkMode()
        {
            if (MainPage.darkMode)
            {
                BackgroundColor = Color.Black;
                TextColor = "White";
                MinTC = "White";
                MaxTC = "LightGray";
            }
            else
            {
                BackgroundColor = Color.White;
                TextColor = "Black";
                MinTC = "Black";
                MaxTC = "#222222";
            }
        }

        //Slider value changed
        void OnValueChanged(object sender, ValueChangedEventArgs args)
        {
            if (args.NewValue < args.OldValue)
            {
                if (Text.Length < args.NewValue + 1) UpdateText((int)(Math.Floor(args.NewValue) - Text.Length)); ;
                Text = Text.Substring(0, (int)args.NewValue);
            }else
            {
                UpdateText((int)(Math.Floor(args.NewValue) - Math.Floor(args.OldValue)));
            }
        }

        //generates random characters based on the amount requested
        void UpdateText(int val)
        {
            for (int i = 0; i < val; i++)
            {
                Random random = new Random();
                Text += (char)random.Next(33, 126); //gets a random character from the ascii table
            }
        }
    }
}