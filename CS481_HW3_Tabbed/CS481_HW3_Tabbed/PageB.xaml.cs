﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3_Tabbed
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class PageB : ContentPage
	{

        private string rotation = "0";
        public string ImgRotation
        {
            get
            {
                return rotation;
            }
            set
            {
                rotation = value;
                OnPropertyChanged("ImgRotation");
            }

        }

        //slider track color (before thumb)
        private string minTC = "Black";
        public string MinTC
        {
            get
            {
                return minTC;
            }
            set
            {
                minTC = value;
                OnPropertyChanged("MinTC");
            }

        }

        //slider track color (after thumb)
        private string maxTC = "#222222";
        public string MaxTC
        {
            get
            {
                return maxTC;
            }
            set
            {
                maxTC = value;
                OnPropertyChanged("MaxTC");
            }

        }

        public PageB ()
		{
			InitializeComponent ();
        }

        void OnAppearing(object sender, EventArgs args)
        {
            BindingContext = this;
            Debug.WriteLine("Page B has appeared.");
            CheckDarkMode();
        }

        void OnDisappearing(object sender, EventArgs args)
        {
            Debug.WriteLine("Page B has disappeared.");
        }

        void ToggleDarkMode(object sender, EventArgs args)
        {
            MainPage.darkMode = !MainPage.darkMode;
            CheckDarkMode();
        }

        void CheckDarkMode()
        {
            if (MainPage.darkMode)
            {
                BackgroundColor = Color.Black;
                MinTC = "White";
                MaxTC = "LightGray";
            }
            else
            {
                BackgroundColor = Color.White;
                MinTC = "Black";
                MaxTC = "#222222";
            }
        }
    }
}